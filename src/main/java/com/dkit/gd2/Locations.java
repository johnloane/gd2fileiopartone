package com.dkit.gd2;

import java.io.*;
import java.util.*;

public class Locations implements Map<Integer, Location> {
    private static final Map<Integer, Location> locations = new HashMap<Integer, Location>();

    public static void main(String[] args) throws IOException{
        try(ObjectOutputStream locationsFile = new ObjectOutputStream((new BufferedOutputStream(new FileOutputStream("locations.dat"))))){
            for(Location location : locations.values()){
                locationsFile.writeObject(location);
            }
        }
//        try(DataOutputStream locationsFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("locations.dat")))){
//            for(Location location : locations.values()){
//                locationsFile.writeInt(location.getLocationID());
//                locationsFile.writeUTF(location.getDescription());
//                System.out.println("Writing location " + location.getLocationID() + " : " + location.getDescription());
//                System.out.println("Writing " + (location.getExits().size() - 1) + "exits");
//                locationsFile.writeInt(location.getExits().size() - 1);
//                for(String direction : location.getExits().keySet()){
//                    if(!direction.equalsIgnoreCase("Q")){
//                        System.out.println("\t\t" + direction + ", " + location.getExits().get(direction));
//                        locationsFile.writeUTF(direction);
//                        locationsFile.writeInt(location.getExits().get(direction));
//                    }
//                }
//
//            }
//        }
        //FileWriter locationsFile = null;
        //try with resources
//        try(BufferedWriter locationsFile = new BufferedWriter(new FileWriter("locations.txt")); BufferedWriter directionsFile = new BufferedWriter(new FileWriter("directions.txt"))) {
//            for (Location location : locations.values()) {
//                locationsFile.write(location.getLocationID() + ", " + location.getDescription() + "\n");
//                for(String direction : location.getExits().keySet()){
//                    directionsFile.write(location.getLocationID() + ", " + direction + ", " + location.getExits().get(direction) + "\n");
//                }
//            }
//        }
//        try {
//            locationsFile = new FileWriter("locations.txt");
//            for(Location location : locations.values()){
//                locationsFile.write(location.getLocationID() + ", " + location.getDescription() + "\n");
//            }
//        }
//        finally{
//            System.out.println("In the finally block");
//            try {
//                if(locationsFile != null) {
//                    System.out.println("Attempting to close the file");
//                    locationsFile.close();
//                }
//            }catch(IOException e){
//                e.printStackTrace();
//            }
//        }
    }

    static {
        try(ObjectInputStream locationsFile = new ObjectInputStream(new BufferedInputStream(new FileInputStream("locations.dat")))){
            boolean eof = false;
            while(!eof){
                try{
                    Location location = (Location) locationsFile.readObject();
                    System.out.println("Read location " + location.getLocationID() + " : " + location.getDescription());
                    System.out.println("Found " + location.getExits().size() + " exits");
                    locations.put(location.getLocationID(), location);
                }catch(EOFException e){
                    eof = true;
                }

            }

        }catch(IOException io) {
            System.out.println("IO Exception " + io.getMessage());
        }catch(ClassNotFoundException e){
            e.getMessage();
        }
    }
//        try (DataInputStream locationsFile = new DataInputStream(new BufferedInputStream(new FileInputStream("locations.dat")))) {
//            boolean eof = false;
//            while (!eof) {
//                try {
//                    Map<String, Integer> exits = new LinkedHashMap<>();
//                    int locationID = locationsFile.readInt();
//                    String description = locationsFile.readUTF();
//                    int numExits = locationsFile.readInt();
//                    System.out.println("Read location " + locationID + " : " + description);
//                    System.out.println("Found " + numExits + " exits");
//                    for (int i = 0; i < numExits; i++) {
//                        String direction = locationsFile.readUTF();
//                        int destination = locationsFile.readInt();
//                        exits.put(direction, destination);
//                        System.out.println("\t\t + direction " + direction + ", " + destination);
//                    }
//                    locations.put(locationID, new Location(locationID, description, exits));
//                } catch (EOFException e) {
//                    eof = true;
//                }
//            }
//        } catch (IOException io) {
//            System.out.println("IO Exception");
//        }
//    }
//        try(Scanner sc = new Scanner(new BufferedReader(new FileReader("locations_big.txt")))){
//            sc.useDelimiter(",");
//            while(sc.hasNextLine()){
//                int loc = sc.nextInt();
//                sc.skip(sc.delimiter());
//                String description = sc.nextLine();
//                description = description.trim();
//                System.out.println("Imported loc: " + loc + ": " + description);
//                Map<String, Integer> tempExit = new HashMap<>();
//                locations.put(loc, new Location(loc, description, tempExit));
//            }
//        }catch(IOException e){
//            e.printStackTrace();
//        }
//
//        //Read the exits
//        try(Scanner sc = new Scanner(new BufferedReader(new FileReader("directions_big.txt")))){
//            sc.useDelimiter(",");
//            while(sc.hasNextLine()){
//                int location = sc.nextInt();
//                sc.skip(sc.delimiter());
//                String direction = sc.next();
//                direction = direction.trim();
//                sc.skip(sc.delimiter());
//                String dest = sc.nextLine();
//                int destination = Integer.parseInt(dest.trim());
//                String input = sc.nextLine();
//                String[] data = input.split(",");
//                int location = Integer.parseInt(data[0]);
//                String direction = data[1].trim();
//                int destination = Integer.parseInt(data[2].trim());
//
//                System.out.println(location + ": " + direction + ": " + destination);
//                Location loc = locations.get(location);
//                loc.addExit(direction, destination);
//            }
//        }catch(IOException e){
//            e.printStackTrace();
//        }



//        Map<String, Integer> tempExit = new HashMap<String, Integer>();
//        locations.put(0, new Location(0, "You are sitting in front of a computer learning Java", null));
//
//        tempExit.put("W", 2);
//        tempExit.put("E", 3);
//        tempExit.put("S", 4);
//        tempExit.put("N", 5);
//
//        locations.put(1, new Location(1, "You are standing at the end of a road before a small brick building", tempExit));
//
//        tempExit = new HashMap<String, Integer>();
//        tempExit.put("N", 5);
//        locations.put(2, new Location(2, "You are at the top a hill", tempExit));
//
//        tempExit = new HashMap<String, Integer>();
//        tempExit.put("W", 1);
//        locations.put(3, new Location(3, "You are on a beach", tempExit));
//
//        tempExit = new HashMap<String, Integer>();
//        tempExit.put("N", 1);
//        tempExit.put("W", 2);
//        locations.put(4, new Location(4, "You are in an office", tempExit));
//
//        tempExit = new HashMap<String, Integer>();
//        tempExit.put("S", 1);
//        tempExit.put("W", 2);
//        locations.put(5, new Location(5, "You are at an old abandoned run down church and you see a beach and an office in the distance", tempExit));

    public int size() {
        return locations.size();
    }

    public boolean isEmpty() {
        return locations.isEmpty();
    }

    public boolean containsKey(Object key) {
        return locations.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return locations.containsValue(value);
    }

    public Location get(Object key) {
        return locations.get(key);
    }

    public Location put(Integer key, Location value) {
        return locations.put(key, value);
    }

    public Location remove(Object key) {
        return locations.remove(key);
    }

    public void putAll(Map<? extends Integer, ? extends Location> m) {

    }

    public void clear() {
        locations.clear();
    }

    public Set<Integer> keySet() {
        return locations.keySet();
    }

    public Collection<Location> values() {
        return locations.values();
    }

    public Set<Entry<Integer, Location>> entrySet() {
        return locations.entrySet();
    }
}
